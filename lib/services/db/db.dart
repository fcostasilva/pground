import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';
import 'globals.dart';

class Document<T> {
  final Firestore _db = Firestore.instance;
  final String path;
  DocumentReference ref;

  Document({this.path}) {
    ref = _db.document(path);
  }

  Future<T> getData() {
    return ref.get().then((v) => Global.models[T](v.data) as T);
  }

  Stream<T> streamData() {
    return ref.snapshots().map((v) => Global.models[T](v.data) as T);
  }

  Future<void> insertOrUpdate(Map data) {
    return ref.setData(Map<String, dynamic>.from(data), merge: true);
  }
}

class Collection<T> {
  final Firestore _db = Firestore.instance;
  final String path;
  CollectionReference ref;

  Collection({this.path}) {
    ref = _db.collection(path);
  }

  Future<List<T>> getData() async {
    var snapshots = await ref.getDocuments();
    return snapshots.documents
        .map((doc) => Global.models[T](doc.data) as T)
        .toList();
  }

  Stream<QuerySnapshot> where(Map params) {
    Query query;
    params.entries.forEach((entry) => {
          query = query == null
              ? ref.where(entry.key, isEqualTo: entry.value)
              : query.where(entry.key, isEqualTo: entry.value)
        });

    return query.snapshots(); //TODO find a way to cast stream to generic object
  }

  Stream<Iterable<T>> streamData() {
    var snapshots = ref.snapshots();
    return snapshots.map(
        (list) => list.documents.map((doc) => Global.models[T](doc.data) as T));
  }
}

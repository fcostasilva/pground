class Category {
  String name;
  String description;

  Category({this.name, this.description});
  Category.fromMap(Map data) {
    name = data['name'];
    description = data['description'] ?? '';
  }
}

class Game {
  String uid;
  String name;
  String img;
  int maxNumPlayers;

  Game({this.uid, this.name, this.img, this.maxNumPlayers});
  Game.fromMap(Map data) {
    uid = data['uid'];
    name = data['name'];
    img = data['img'] ?? '';
    maxNumPlayers = data['maxNumPlayers'] ?? 1;
  }
}

class GameRoom {
  String uid;
  String gameId;
  var players = [];
  String status;

  GameRoom({this.uid, this.gameId, this.players, this.status});
  GameRoom.fromMap(Map data) {
    uid = data['uid'];
    gameId = data['game_id'];
    players = data['players'] ?? [];
    status = data['status'] ?? "OPEN";
  }
}

class Report {
  String uid;
  int total;

  Report({this.uid, this.total});

  factory Report.fromMap(Map data) {
    return Report(
      uid: data['uid'],
    );
  }
}

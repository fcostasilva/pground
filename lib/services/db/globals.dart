import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'db.dart';
import 'models.dart';

class Global {
  static final Map models = {
    Category: (data) => Category.fromMap(data),
    Game: (data) => Game.fromMap(data),
    GameRoom: (data) => GameRoom.fromMap(data),
    Report: (data) => Report.fromMap(data),
  };

  static final Collection<Game> allGames = Collection<Game>(path: 'games');
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart';

class ImageUtil {
  static Future<CachedNetworkImage> getImage(
      BuildContext context, String imagePath) async {
    CachedNetworkImage image;
    await FireStorageService.loadFromStorage(context, imagePath)
        .then((downloadUrl) {
      image = CachedNetworkImage(
        placeholder: (context, url) => Container(
          height: 30,
          child: CircularProgressIndicator()),
        imageUrl: downloadUrl.toString(),
        fit: BoxFit.scaleDown,
      );
    });

    return image;
  }
}

class FireStorageService extends ChangeNotifier {
  FireStorageService._();
  FireStorageService();

  static Future<dynamic> loadFromStorage(
      BuildContext context, String image) async {
    return await FirebaseStorage.instance.ref().child(image).getDownloadURL();
  }
}


import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:game/nav/maratona.dart';

class AppBottomNav extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      showSelectedLabels: false,
      showUnselectedLabels: false,
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.arrow_upward, size: 20),
            title: Padding(padding: EdgeInsets.all(0.0))),
        BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.award, size: 20),
            title: Padding(padding: EdgeInsets.all(0.0))),
        BottomNavigationBarItem(
            icon: Icon(Icons.watch_later, size: 20),
            title: Padding(padding: EdgeInsets.all(0.0))),
           BottomNavigationBarItem(
            icon: Icon(Icons.exit_to_app, size: 20),
            title: Padding(padding: EdgeInsets.all(0.0))),  
      ].toList(),
      fixedColor: Colors.deepPurple[200],
      onTap: (int idx) {
        switch (idx) {
          case 0:
            Navigator.pushNamed(context, '/home');
            break;
          case 1:
            Navigator.pushNamed(context, '/ranking');
            break;
          case 2:
            Navigator.push(context, MaterialPageRoute(
              builder: (BuildContext context) => MaratonaPage()
            ));
            break;
          case 3:
            Navigator.pushNamed(context, '/login');
            break;
        }
      },
    );
  }
}
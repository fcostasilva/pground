import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class GameTopBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width / 4.5,
            child: DecoratedBox(
                icon: FontAwesomeIcons.heart, text: "+5", color: Colors.red),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            width: MediaQuery.of(context).size.width / 2.7,
            child: DecoratedBox(
                icon: FontAwesomeIcons.star,
                text: "Malhação",
                color: Colors.red),
          ),
        ],
      ),
    );
  }
}

class DecoratedBox extends StatelessWidget {
  final IconData icon;
  final String text;
  final Color color;

  const DecoratedBox({Key key, this.icon, this.text, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Chip(
      shape: StadiumBorder(
        side: BorderSide(color: color, width: 3),
      ),
      label: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Icon(
            icon,
            color: color,
          ),
          Text(
            text,
            style: GoogleFonts.roboto(
                color: color, fontWeight: FontWeight.bold, fontSize: 18),
          ),
        ],
      ),
    );
  }
}
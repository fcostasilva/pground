import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class ImageWithTextOver extends StatelessWidget {
  final CachedNetworkImage image;
  final String text;
  final double fontSize;
  final bool opaque;

  const ImageWithTextOver(
      {this.image, this.text, this.fontSize, this.opaque = false});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: this.opaque ? Opacity(opacity: 0.5, child: image) : image,
        ),
        Center(
            child: Text(
          text,
          style: GoogleFonts.roboto(
            textStyle: TextStyle(fontSize: fontSize, shadows: <Shadow>[
              Shadow(
                offset: Offset(1.0, 1.0),
                blurRadius: 0.5,
                color: Color.fromARGB(255, 0, 0, 0),
              ),
            ]),
          ),
        )),
      ],
    );
  }
}

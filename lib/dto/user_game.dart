import 'package:firebase_auth/firebase_auth.dart';
import 'package:game/services/db/models.dart';

class UserGameDTO {
  final FirebaseUser user;
  final Game game;

  UserGameDTO({this.user, this.game});
}

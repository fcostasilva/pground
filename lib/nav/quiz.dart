import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:game/components/app_bottom.dart';
import 'package:game/components/components.dart';
import 'package:game/dto/user_game.dart';
import 'package:game/nav/quiz_resistencia.dart';
import 'package:game/services/db/db.dart';
import 'package:game/services/db/globals.dart';
import 'package:game/services/db/models.dart';

class QuizPage extends StatefulWidget {
  QuizPageState createState() => QuizPageState();
}

class QuizPageState extends State<QuizPage> {
  GameRoom room;
  Map queryFilters;
  int players = 0;

  bool prepared = false;
 
  void prepare(BuildContext context) {
    if(prepared) return;
    UserGameDTO userGame = ModalRoute.of(context).settings.arguments;

    queryFilters = {"status": "OPEN", "game_id": userGame.game.uid};

    Collection<GameRoom>(path: 'game_rooms')
        .where(queryFilters)
        .first
        .then(
          (QuerySnapshot item) => {
            setState(
              () => {room = Global.models[GameRoom](item.documents[0].data)},
            ),
          },
        )
        .whenComplete(
          () => {
            Document<GameRoom>(path: "game_rooms/${room.uid}")
                .insertOrUpdate(mergePlayers(userGame.user, room.players))
                .whenComplete(() => {
                      setState(
                        () => {prepared = true},
                      ),
                    })
          },
        );
  }

  Map mergePlayers(FirebaseUser user, List players) {
    var filteredPlayers =
        players.where((player) => user.uid != player).where((player) => player != null && player != "" ).toList();

    filteredPlayers.add(user.uid);
    return {"players": filteredPlayers};
  }

  @override
  Widget build(BuildContext context) {
    prepare(context);

    return room == null
        ? Scaffold(body: Text("Carregando..."))
        : Scaffold(
            appBar: AppBar(
              title: GameTopBar(),
            ),
            bottomNavigationBar: AppBottomNav(),
            body: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: <Widget>[
                    Text("Salas disponíveis: "),
                    SizedBox(
                      height: 10,
                    ),
                    StreamBuilder<QuerySnapshot>(
                        stream: Collection<GameRoom>(path: 'game_rooms')
                            .where(queryFilters),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return const Text("Carregando...");
                          }

                          return Container(
                            color: Colors.black38,
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height * 0.7,
                            child: ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, index) {
                                return addRoom(context, index,
                                    snapshot.data.documents[index]);
                              },
                            ),
                          );
                        }),
                  ],
                ),
              ),
            ),
          );
  }

  Widget addRoom(BuildContext context, int index, DocumentSnapshot item) {
    GameRoom room = Global.models[GameRoom](item.data);
    int qtdPlayers = room.players.length;

   players = qtdPlayers;

    String message = "";
    if (qtdPlayers == 1) {
      message = "nenhum jogador online.";
    } else if (qtdPlayers == 2) {
      message = "1 jogador esperando para o desafio! Vai encarar essa?";
    } else {
      message = "$qtdPlayers jogadores esperando o desafio! Vai encarar essa?";
    }

    return Container(
      padding: EdgeInsets.fromLTRB(10, 30, 10, 10),
      height: 140,
      width: double.maxFinite,
      child: GestureDetector(
        onTap: () => play(),
        child: Card(
          elevation: 5,
          child: Padding(
            padding: EdgeInsets.all(7),
            child: Stack(children: <Widget>[
              Align(
                alignment: Alignment.centerRight,
                child: Stack(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(left: 10, top: 5),
                        child: roomMessage(index, message))
                  ],
                ),
              )
            ]),
          ),
        ),
      ),
    );
  }

  void play() {
    Navigator.push(context, MaterialPageRoute(
        builder: (BuildContext context) => QuizResistenciaPage(
          pts: 0, qtdPlayers: players,
        )
    ),
    );
  }

  Widget roomMessage(int index, String message) {
    return Center(
      child: Column(
        children: <Widget>[
          Text("Sala ${index + 1}",
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              )),
          SizedBox(
            height: 15,
          ),
          Text(
            message,
            style: TextStyle(
                color: Colors.grey,
                fontStyle: FontStyle.italic,
                fontSize: 13,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}

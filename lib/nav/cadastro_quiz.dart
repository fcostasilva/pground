import 'package:flutter/material.dart';

class CadastroQuizPage extends StatefulWidget{
   createState() => CadastroQuizPageState();
}

class CadastroQuizPageState extends State<CadastroQuizPage> {
  @override
  Widget build(BuildContext context){
      return Scaffold(
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children:[
              SizedBox(height: 50,),

              SizedBox(
                height: 250,
                child: Image.asset(
                  "assets/imgs/noimage.png",
                ),    
              ),

              SizedBox(height: 100,),
              Text('Participar do quiz?',style:TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),

              SizedBox(height: 10,),

              FlatButton(
                padding: EdgeInsets.all(10),
                child: Text('Sim', 
                          style:TextStyle(fontSize: 20,
                        ),
                      ),
                color: Colors.red,
                onPressed: (){
                    Navigator.pushReplacementNamed(context, '/cadastro_quiz_two');
                },
              ),

              FlatButton(
                padding: EdgeInsets.all(10),
                child: Text('Não', 
                          style:TextStyle(fontSize: 20,
                        ),
                      ),
                color: Colors.red,
                onPressed: (){
                    Navigator.pushReplacementNamed(context, '/');
                },
              ),
            ]
          ),
        ),
      );
  }
}
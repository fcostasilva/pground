import 'package:flutter/material.dart';

class CadastroQuizTwoPage extends StatefulWidget{
   createState() => CadastroQuizTwoPageState();
}

class CadastroQuizTwoPageState extends State<CadastroQuizTwoPage> {

 int _value1 = 0;
 int _value2 = 0;

 void _setvalue1(int value) => setState(() => _value1 = value);
 void _setvalue2(int value) => setState(() => _value2 = value);

 Widget makeRadios() {
   List<Widget> list = new List<Widget>();

   for(int i=0; i<3; i++){
     list.add(new Radio(value: i, groupValue: _value1, onChanged: _setvalue1));
   }

   Column column = new Column(
     children: list,
   );

   return column;
 }

 Widget makeRadioTiles() {
   List<Widget> list = new List<Widget>();

   for(int i = 0; i < 3; i++){
     list.add(new RadioListTile(
       value: i,
       groupValue: _value2,
       onChanged: _setvalue2,
       activeColor: Colors.green,
       controlAffinity: ListTileControlAffinity.trailing,
       title: new Text('Item: One'),
       subtitle: new Text('sub title'),

     ));
   }

   Column column = new Column(children: list,);
   return column;
 }

  @override
  Widget build(BuildContext context){
      return Scaffold(
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children:[
              SizedBox(height: 50,),

              SizedBox(
                height: 200,
                child: Image.asset(
                  "assets/imgs/noimage.png",
                ),    
              ),

              SizedBox(height: 50,),
              Text('O que você acha da atual condição econômica do país?',style:TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),

              SizedBox(height: 10,),
              
              makeRadioTiles(),

              FlatButton(
                padding: EdgeInsets.all(10),
                child: Text('Próxima', 
                          style:TextStyle(fontSize: 20,
                        ),
                      ),
                color: Colors.red,
                onPressed: (){
                    Navigator.pushReplacementNamed(context, '/cadastro_quiz_three');
                },
              ),
            ]
          ),
        ),
      );
  }
}
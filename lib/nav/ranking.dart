import 'package:flutter/material.dart';
import 'package:game/components/app_bottom.dart';

class RankingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ranking - TOP 5', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),),
        backgroundColor: Colors.yellowAccent,
      ),

      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: buildItems(context)
      ),

      bottomNavigationBar: AppBottomNav(),
    );
  }

  Widget buildItems(BuildContext context) {
    final titles = [
      'Joana Almeida',
      'Bárbara Cerqueira',
      'Jacson dos Santos',
      'Carlos Albuquerque',
      'Gabriel Costa',
      'Lisandra Rodrigues',
      'Jorge Oliveira',
      'Matheus Bastos',
      'walk'
    ];

    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context, index) {
        return Container(
          width: 120,
          height: 100,
          child:  Card(
            child: ListTile(
              leading: Text("${201 - index} pontos"),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                    
                    Text(titles[index],
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.yellowAccent,
                        ),
                    ),

                    ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: Image.asset('assets/imgs/avatar.jpg', height: 50,),
                    ),

                ],
              ),
            ),
          ),

        );
      },
    );
  }
}

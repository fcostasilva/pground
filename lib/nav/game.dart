import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:game/components/app_bottom.dart';
import 'package:game/components/components.dart';
import 'package:game/components/image_text_overlay.dart';
import 'package:game/dto/user_game.dart';
import 'package:game/nav/quiz_resistencia.dart';
import 'package:game/services/db/models.dart';
import 'package:provider/provider.dart';

class GamePage extends StatelessWidget {
  const GamePage({Key key, this.game, this.image}) : super(key: key);
  final Game game;
  final CachedNetworkImage image;

  @override
  Widget build(BuildContext context) {
    final FirebaseUser user = Provider.of<FirebaseUser>(context);

    final UserGameDTO userGame = UserGameDTO(user: user, game: game);

    return Scaffold(
      appBar: AppBar(
        title: GameTopBar(),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height / 4,
                width: MediaQuery.of(context).size.width,
                child: ImageWithTextOver(
                  fontSize: 30,
                  image: image,
                  text: game.name,
                  opaque: true,
                ),
              ),
              
              if(game.name == 'Prova de Resistência')
                resistencia(context),
              
              if(game.name == 'Fogo no Parquinho')
                parquino(context),
              
              if(game.name == 'Bate-Volta')
                bateVolta(context),

              SizedBox(
                height: 20
              ),
              Container(
                width: 150,
                height: 40,
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      side: BorderSide(color: Colors.purpleAccent)),
                  padding: EdgeInsets.all(10),
                  color: Colors.purpleAccent,
                  child: Text('Jogar',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 14)),
                  onPressed: () => {
                    if(game.name == 'Prova de Resistência'){
                        Navigator.push(context, MaterialPageRoute(
                            builder: (BuildContext context) => QuizResistenciaPage(
                              pts: 0,
                            )
                          ),
                        )
                      }
                    else
                      Navigator.pushNamed(context, '/quiz', arguments: userGame)
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: AppBottomNav(),
    );
  }

  Widget resistencia(BuildContext context){
     return Container(
        child: Column(
            children: <Widget>[
              Container(
                  height: MediaQuery.of(context).size.height / 5,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Melhor série: 25 ',
                          style: TextStyle(
                              fontSize: 45,
                              fontWeight: FontWeight.bold,
                              color: Colors.white38),
                        ),
                      ]
                  ),
              ),

              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[

                     Text('1 resposta correta 5 pts',
                          style: TextStyle(
                            color: Colors.purpleAccent,
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                          )),
                      Text(
                        'Multiplicador de ponto x2',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.purpleAccent,
                            fontSize: 30),
                      ),
                      
                  ],
                ),
              ),
                
            ],
        ),
     );
  }

  Widget parquino(BuildContext context){
     return Container(
        child: Column(
            children: <Widget>[
              Container(
                  height: MediaQuery.of(context).size.height / 5,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Acaba em ',
                          style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),

                        Text(
                          '4d 5h 30m ',
                          style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.pink),
                        ),
                      ]
                  ),
              ),

              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[

                      Text('Pontos: 750',
                          style: TextStyle(
                            color: Colors.white38,
                            fontWeight: FontWeight.bold,
                            fontSize: 45,
                          )
                      ),
                      
                  ],
                ),
              ),
                
            ],
        ),
     );
  }

  Widget bateVolta(BuildContext context){
     return Container(
        child: Column(
            children: <Widget>[

              Container(
                  padding: EdgeInsets.all(20),
                  height: MediaQuery.of(context).size.height / 3,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[

                        cardAvatar(context,'assets/imgs/avatar_one.jpg', 'João Alenquer',),
                        cardAvatar(context,'assets/imgs/avatar_two.jpg', 'Maria Joana'),
                        cardAvatar(context,'assets/imgs/avatar_three.jpg', "Joana D'Arc"),

                      ]
                  ),
              ),

            ],
        ),
     );
  }

  Widget cardAvatar(BuildContext context,image,nome){
     return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(5),
        width: MediaQuery.of(context).size.width/4,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(image, height: 70, width: 100,),
              SizedBox(height: 10,),
              Text(nome),
            ],
        ),  
      );
   }
}
import 'package:flutter/material.dart';
import 'package:game/components/popup.dart';

class CadastroPage extends StatefulWidget{
   createState() => CadastroPageState();
}

class CadastroPageState extends State<CadastroPage> {

  Popup popup = new Popup();

  final _nomeController = TextEditingController();

  @override
  Widget build(BuildContext context){
      return Scaffold(
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children:[
              SizedBox(height: 50,),

              SizedBox(
                height: 250,
                child: Image.asset(
                  "assets/imgs/noimage.png",
                ),    
              ),

              SizedBox(height: 100,),
              Text('Como posso te chamar?',style:TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),

              SizedBox(height: 10,),
              TextFormField(
                controller: _nomeController,
                style: TextStyle(
                  color: Colors.white
                ),

                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.7),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(25.7),
                  ),
                  hintText: 'Enter text',
                ),
              ),

              SizedBox(height: 10,),
              FlatButton(
                padding: EdgeInsets.all(10),
                child: Text('Avançar', 
                          style:TextStyle(fontSize: 20,
                        ),
                      ),
                color: Colors.red,
                onPressed: (){
                    if(_nomeController.text != '')
                      Navigator.pushReplacementNamed(context, '/cadastro_quiz');
                   else
                      popup.open(context, 'Campo Obrigatório', 'Por favor infome seu nome!');
                },
              ),
            ]
          ),
        ),
      );
  }
}
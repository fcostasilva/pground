import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:game/components/popup.dart';
import 'package:game/services/authentication.dart';

class LoginPage extends StatefulWidget {
  createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  AuthService auth = AuthService();

  final _emailController = TextEditingController();
  final _passController = TextEditingController();

  @override
  void initState() {
    super.initState();
    auth.getUser.then(
      (user) {
        if (user != null) {
          Navigator.pushReplacementNamed(context, '/home');
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Colors.black54,
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 40,),

            SizedBox(
              height: 150.0,
              child: Image.asset(
                "assets/imgs/logo.png",
                fit: BoxFit.contain,
              ),              
            ),
            
            Text(
              'Olá,',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize:25, 
                height: 4
              ),
              textAlign: TextAlign.start,
            ),

            Text(
              'Seja bem-vindo (a)!',
              style: TextStyle(
                fontSize:22, 
                height: 1,
              ),
              textAlign: TextAlign.start
            ),

            SizedBox(height: 10,),
            
            Container(
                padding: EdgeInsets.all(20),
                width:320,
                child:Column(
                  children: <Widget>[

                    TextFormField(
                      controller: _emailController,
                      keyboardType: TextInputType.text,
                      validator: (String value) {
                        return value.contains('@') ? null : 'Este não é um e-mail válido!';
                      },
                      style: TextStyle(
                        color: Colors.white
                      ),
                      decoration: InputDecoration(
                        prefixIcon: Padding(
                          padding: EdgeInsets.all(0.0),
                          child: Icon(FontAwesomeIcons.envelope), // icon is 48px widget.
                        ),
                        filled: true,
                        fillColor: Colors.white10,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        hintText: 'E-mail',
                      ),
                    ),

                    SizedBox(height: 8,),

                    TextFormField(
                      obscureText: true,
                      controller: _passController,
                      keyboardType: TextInputType.number,
                      style: TextStyle(
                        color: Colors.white
                      ),
                      decoration: InputDecoration(
                        prefixIcon: Padding(
                          padding: EdgeInsets.all(0.0),
                          child: Icon(FontAwesomeIcons.key), // icon is 48px widget.
                        ),
                        filled: true,
                        fillColor: Colors.white10,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        hintText: 'Senha',
                      ),
                    ),

                    SizedBox(height: 25,),

                    FlatButton.icon(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        side: BorderSide(color: Colors.red)
                      ),
                      padding: EdgeInsets.all(10),
                      icon: Icon(FontAwesomeIcons.signInAlt, color: Colors.white),
                      color: Colors.red,
                      label: Expanded(
                        child: Text('Entrar', textAlign: TextAlign.center, style:TextStyle(fontSize: 14)),
                      ),
                      onPressed: () async {
                        auth.login(_emailController.text,_passController.text).then( (FirebaseUser user) {
                          Navigator.pushReplacementNamed(context, '/home');
                        }).catchError((e) =>{
                            Popup().open(context, e.code, e.message)
                        });
                      },
                    ),
                    
                    SizedBox(height: 10,),

                    // FlatButton.icon(
                    //   shape: new RoundedRectangleBorder(
                    //     borderRadius: new BorderRadius.circular(25.0),
                    //   ),
                    //   padding: EdgeInsets.all(10),
                    //   icon: Icon(FontAwesomeIcons.google, color: Colors.black),
                    //   color: Colors.white,
                    //   label: Expanded(
                    //     child: Text('Login com Google', textAlign: TextAlign.center, style:TextStyle(fontSize: 14, color: Colors.black)),
                    //   ),
                    //   onPressed: auth.googleSignIn,
                    // ),
        
                     FlatButton.icon(
                       icon: Icon(FontAwesomeIcons.google, color: Colors.black),
                       label: Expanded(
                         child: Text('Continuar anonimamente', textAlign: TextAlign.center, style:TextStyle(fontSize: 14, color: Colors.white)),
                       ),
                       onPressed: doLogin
                     ),

                    Text('Não possui uma conta ?', style: TextStyle(fontWeight: FontWeight.bold)),
                    FlatButton(
                      child: Text('Cadastre-se'),
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, '/cadastro');
                      },
                    )
                  ],
                ),
               ),
          ],
        ),
      ),
    );
  }

  doLogin() async{    
          var user = await auth.anonLogin();
          if (user != null) {
            Navigator.pushReplacementNamed(context, '/home');
          }
        
  }
}
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TryAgainPage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
      return Scaffold(
          appBar: AppBar(
             title: Column(
                children: <Widget>[
                   Image.asset('assets/imgs/logo_appbar.png',height: 50,)
                ],
             ),
          ),

          body: Container(
             alignment: Alignment.center,
             color: Colors.black,
             padding: EdgeInsets.all(50),
             child: Column(
                children: <Widget>[
                    Text('Deseja tentar novamente?',
                        style: TextStyle(
                           color: Colors.white,
                           fontWeight: FontWeight.bold,
                           fontSize: 35
                        )
                    ),

                    SizedBox(height: 40,),

                    Text('Assita um anúncio para tentar outra vez.',
                        style: TextStyle(
                           color: Colors.white,
                           fontWeight: FontWeight.bold,
                           fontSize: 25
                        )
                    ),

                    SizedBox(height: 100,),

                    FlatButton.icon(
                      label: Text('Assitir'),
                      color: Colors.red,
                      padding: EdgeInsets.all(10),
                      icon: Icon(FontAwesomeIcons.video),
                      onPressed: (){},
                    )
                ],
             ),
          ),
      );
  }
}
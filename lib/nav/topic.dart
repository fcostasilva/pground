import 'package:flutter/material.dart';
// import 'package:game/components/app_bottom.dart';
import 'package:game/services/authentication.dart';


class TopicsPage extends StatelessWidget {

  final AuthService auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Categorias...')),
      body: Container(
        child: Column(
          children: [
            Text('...'),
            FlatButton(
              child: Text('Sair'),
              color: Colors.red,
              onPressed: () async {
                await auth.signOut();
                Navigator.of(context).pushNamedAndRemoveUntil('/', (route) => false);
              }
            ),
          ]
        ),
      ),
      // bottomNavigationBar: AppBottomNav(),
    );
  }
}

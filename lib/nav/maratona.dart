import 'package:flutter/material.dart';
import 'package:game/nav/maratona_content.dart';

class MaratonaPage extends StatefulWidget{
    createState() => MaratonaPageState();
}

class MaratonaPageState extends State<MaratonaPage> {

    @override 
    Widget build(BuildContext context){
      return Scaffold(
         appBar: AppBar(
            title: Image.asset('assets/imgs/logo_appbar.png', height: 40,),
         ),

         body: Container(
            child: Column(
              children: <Widget>[
                
                Container(
                  padding:  EdgeInsets.all(5),
                  width: MediaQuery.of(context).size.width,
                  color: Colors.yellowAccent,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('Horas ',
                            style: TextStyle(
                                fontSize: 40,
                                color: Colors.black,
                                
                            ),
                        ),

                        Text('Maratonadas',
                            style: TextStyle(
                               fontSize: 40,
                               fontWeight: FontWeight.bold,
                               color: Colors.black
                            ),
                        ), 
                      ],
                  ),
                ),

                Container(
                    height: MediaQuery.of(context).size.height / 4,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                            ClipOval(
                              child: Image.asset('assets/imgs/avatar.jpg', height: 110,),
                            ),

                            Text('Ana Clara',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 20
                                ),
                            )
                        ],
                    ),
                ),

                Container(
                    padding: EdgeInsets.all(10),
                    color: Colors.yellowAccent,
                    height: MediaQuery.of(context).size.height / 9,
                    width: MediaQuery.of(context).size.width / 1.2,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                            Column(
                              children: <Widget>[

                                  Text('Maratona do Mês',
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.black
                                    ),
                                  ),

                                  Text('34h 25m',
                                    style: TextStyle(
                                        fontSize: 25,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),

                              ],
                            ),

                            Column(
                              children: <Widget>[

                                  Text('Total de Pontos',
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.black
                                    ),
                                  ),

                                  Text('600',
                                    style: TextStyle(
                                        fontSize: 25,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),

                              ],
                            ),
                        ],
                    ),
                ),

                SizedBox(height: 20,),

                Container(
                  alignment: Alignment.centerLeft,
                  margin: new EdgeInsets.symmetric(horizontal: 20.0),
                  padding: EdgeInsets.all(10),
                  child: Text('Novelas',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 22
                    ),
                  ),
                ),

                Container(
                    padding: EdgeInsets.all(5),
                    margin: new EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                        children: <Widget>[
                            CardMaratona(image:'assets/imgs/novela_icon_one.jpg', title: 'Fina Estampa'),
                            CardMaratona(image:'assets/imgs/novela_icon_two.png', title: 'Totalmente Demais'),
                            CardMaratona(image:'assets/imgs/novela_icon_three.png', title: 'Novo Mundo'),
                        ],
                    ),
                ),

                
              ],
            ),
         ),
      );
    }
}

class CardMaratona extends StatelessWidget{

    final title;
    final image;

    const CardMaratona({Key key, this.image, this.title}) : super(key : key);

   @override 
   Widget build(BuildContext context){
     return Card(
            color: Colors.transparent,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: InkWell(
                  child: Image.asset(image, height: 70, width: 100,),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(
                      builder : (BuildContext context) => MaratonaContentPage(
                          image: image,
                          title: title
                      )
                    ));
                  },
              ), 
            ),
          );
   }
}
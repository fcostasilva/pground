import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:game/components/popup.dart';
import 'package:game/nav/tente_novamente.dart';

import 'maratona.dart';

class MaratonaContentPage extends StatelessWidget{

    final image;
    final title;

    const MaratonaContentPage({Key key, this.image, this.title}) : super(key : key);

    @override
    Widget build(BuildContext context){
        return Scaffold(
          appBar: AppBar(
            title: Image.asset('assets/imgs/logo_appbar.png', height: 40,),
          ),

          body: ListView(
                padding: EdgeInsets.all(20),
                children: <Widget>[

                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 5,
                    child: Image.asset(image, width: MediaQuery.of(context).size.width,),
                  ),

                  SizedBox(height: 20,),

                  Text(title,
                      style: TextStyle(
                         fontWeight: FontWeight.bold,
                         fontSize: 30,
                         color: Colors.white
                      ),
                  ),

                  SizedBox(height: 20,),

                  Episodes(title: " Ep. 01 Seasson 01", image: image, checked: 'true', color: Colors.green,),
                  SizedBox(height: 5,),
                  Episodes(title: " Ep. 02 Seasson 01", image: image, checked: 'false',),
                  SizedBox(height: 5,),
                  Episodes(title: " Ep. 03 Seasson 01", image: image, checked: 'false',),
                  SizedBox(height: 5,),
                  Episodes(title: " Ep. 04 Seasson 01", image: image, checked: 'false',),
                  SizedBox(height: 5,),
                  Episodes(title: " Ep. 05 Seasson 01", image: image, checked: 'false',),

                ],
          ),

        );
    }
}

class Episodes extends StatelessWidget{

    final title;
    final image;
    final checked;
    final color;

    const Episodes({Key key, this.image, this.title, this.checked, this.color}):super(key :key);

   @override
   Widget build(BuildContext context){
    
     return FlatButton.icon(
        padding: EdgeInsets.all(15),
        color: Colors.black38,
        label: Row(
          children: <Widget>[
              Image.asset(image, height: 50,),
              Text(title)
          ],
        ),
        icon: Icon(FontAwesomeIcons.checkCircle, color: color,),
        onPressed: (){
            if(checked == 'false'){
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) => MaratonaQuiz()
              ));
            }else{
               Popup().open(context, 'Concluído', 'Você já concluiu este quiz!');
            }
        },
      );
   }
}

class MaratonaQuiz extends StatelessWidget{

   @override
   Widget build(BuildContext context){
     return Scaffold(
          appBar: AppBar(
            title: Image.asset('assets/imgs/logo_appbar.png', height: 40,),
          ),

          body: Container(
             alignment: Alignment.center,
             child: Column(
                mainAxisAlignment:  MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  
                  Container(
                      alignment: Alignment.centerRight,
                      child: FlatButton.icon(
                        label: Text(''),
                        icon: Icon(FontAwesomeIcons.solidWindowClose, color: Colors.yellowAccent, size: 30,),
                        onPressed: () {},
                      ),
                  ),

                  Text('Vamos de quiz?',
                      style: TextStyle(
                         fontSize: 35,
                         fontWeight: FontWeight.bold,
                         color: Colors.white
                      ),
                  ),

                  SizedBox(height: 20,), 

                  Container(
                    padding: EdgeInsets.all(25),
                    child:Text('Por questões de segurança, para validar suas horas asistidas é necessário que você responda uma pergunta sobre o episódio assitido!',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                         fontSize: 20,
                         fontWeight: FontWeight.bold,
                         color: Colors.white
                      ),
                    ),
                  ),

                  FlatButton(
                    child: Text('Responder',
                        style: TextStyle(
                            color: Colors.black87,
                            fontWeight: FontWeight.bold
                        ),
                    ),
                    color: Colors.yellowAccent,
                    padding: EdgeInsets.all(20),
                    onPressed: (){
                        Navigator.push(context, MaterialPageRoute(
                            builder: (BuildContext context) => MaratonaQuizPergunta()
                        ));
                    },
                  ),
                  
                ],
             ),
          ), 
      );
   }
}

class MandouBem extends StatelessWidget{

   @override
   Widget build(BuildContext context){
     return Scaffold(
          appBar: AppBar(
            title: Image.asset('assets/imgs/logo_appbar.png', height: 40,),
          ),

          body: Container(
             alignment: Alignment.center,
             child: Column(
                mainAxisAlignment:  MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  
                  Container(
                      alignment: Alignment.centerRight,
                      child: FlatButton.icon(
                        label: Text(''),
                        icon: Icon(FontAwesomeIcons.solidWindowClose, color: Colors.yellowAccent, size: 30,),
                        onPressed: () {
                            Navigator.push(context, MaterialPageRoute(
                               builder: (BuildContext contect) => MaratonaPage()
                            ));
                        },
                      ),
                  ),

                  Text('Mandou Bem!!!',
                      style: TextStyle(
                         fontSize: 35,
                         fontWeight: FontWeight.bold,
                         color: Colors.yellowAccent
                      ),
                  ),

                  SizedBox(height: 20,), 

                  Container(
                    padding: EdgeInsets.all(25),
                    child:Text('Suas horas maratonadas viram pontos quando você acumula!',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                         fontSize: 20,
                         fontWeight: FontWeight.bold,
                         color: Colors.white
                      ),
                    ),
                  ),

                  Container(
                    padding: EdgeInsets.all(25),
                    child:Text('Continue assistindo os conteúdos do GloboPlay para arrasar em todas as cateogrias do plauground!',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                         fontSize: 20,
                         fontWeight: FontWeight.bold,
                         color: Colors.white
                      ),
                    ),
                  ),
                  
                ],
             ),
          ), 
      );
   }
}

class MaratonaQuizPergunta extends StatelessWidget{
   @override
   Widget build(BuildContext context){
      return Scaffold(

          appBar: AppBar(
              title: Image.asset('assets/imgs/logo_appbar.png',height: 40,),
          ),

          body: Container(
             child: Column(
                children: <Widget>[

                    Container(
                      padding: EdgeInsets.all(30),
                      color: Colors.white54,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('Qual é o nome da cantora que faz o show no presídio onde o irmão do Dante está detido?', 
                            style: TextStyle( 
                              color: Colors.white,
                              fontSize: 25,
                              fontWeight: FontWeight.bold
                            ) 
                          )
                        ],
                      ),
                    ),

                    SizedBox(height: 20,),

                    ButtonOpcoes(text: 'Vera',   value:'Vera',    correctValue: 'Sheila',),
                    ButtonOpcoes(text: 'Vilma',  value:'Vilma',   correctValue: 'Sheila',),
                    ButtonOpcoes(text: 'Sheila', value:'Sheila',  correctValue: 'Sheila',),
                    ButtonOpcoes(text: 'Júlia',  value:'Júlia',   correctValue: 'Sheila',),

                ],
             ),
          ),

      );
   }
}


class ButtonOpcoes extends StatelessWidget {
    const ButtonOpcoes({Key key, this.text, this.value,this.correctValue}) : super(key: key);    
    final String text;
    final String value;
    final String correctValue;

    @override
    Widget build(BuildContext context){
      return FlatButton(
        child: Text(text),
        color: Colors.white38,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(25.0),
        ),
        onPressed: () {
            if(value == correctValue){
                  Navigator.push(context, MaterialPageRoute(
                      builder: (BuildContext context) => MandouBem()
                    ),
                  );
            }else{
              Navigator.push(context, MaterialPageRoute(
                  builder: (BuildContext context) => TryAgainPage()
              ));
            }
        },
      );
    }
}
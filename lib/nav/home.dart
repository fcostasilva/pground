import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:game/components/components.dart';
import 'package:game/services/db/globals.dart';
import 'package:game/services/db/models.dart';
import 'package:game/services/services.dart';

import 'game.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Game>>(
        future: Global.allGames.getData(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) {
            return Text("Error: Game snapshot");
          }
          if (snapshot.hasData) {
            List<Game> games = snapshot.data;
            return Scaffold(
              
              appBar: AppBar(
                title: Row(
                    children: <Widget>[
                      Image.asset('assets/imgs/logo_appbar.png', fit: BoxFit.cover,height: 40,),
                    ]
                  ),
              ),
              body: ListView(
                padding: const EdgeInsets.all(15.0),
                children: games.map((game) => GameItem(game: game)).toList(),
              ),
              bottomNavigationBar: AppBottomNav(),
            );
          } else {
            return LoadingIndicator();
          }
        });
  }
}

class GameItem extends StatelessWidget {
  final Game game;
  const GameItem({Key key, this.game}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return FutureBuilder<CachedNetworkImage>(
        future: ImageUtil.getImage(context, game.img),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done)
            return Hero(
              tag: game.img,
              child: Card(
                  clipBehavior: Clip.antiAlias,
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => GamePage(
                            game: game,
                            image: snapshot.data,
                          ),
                        ),
                      );
                    },
                    
                    child: Container(
                      height: MediaQuery.of(context).size.height / 4.1,
                      padding: const EdgeInsets.all(5.0),
                      child: ImageWithTextOver(image: snapshot.data, text: game.name, fontSize: 30,)
                    ),
                  )),
            );

          if (snapshot.connectionState == ConnectionState.waiting)
            return Container(
                child: CircularProgressIndicator());

          return Container();
        });
  }
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:game/components/popup.dart';
import 'package:game/services/authentication.dart';

class CadastroEmailPage extends StatefulWidget{
   createState() => CadastroEmailPageState();
}

class CadastroEmailPageState extends State<CadastroEmailPage> {

  Popup popup = new Popup();
  AuthService auth = new AuthService();

  final _emailController = TextEditingController();
  final _senhaController = TextEditingController();

  @override
  Widget build(BuildContext context){
      return Scaffold(
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children:[
              SizedBox(height: 50,),

              SizedBox(
                height: 250,
                child: Image.asset(
                  "assets/imgs/noimage.png",
                ),    
              ),

              SizedBox(height: 50,),
              Text('Gostaria de enviar um presente para você. Mas antes informe um email para contato e proteja seus dados',style:TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),

              SizedBox(height: 30,),
              TextFormField(
                controller: _emailController,
                style: TextStyle(
                  color: Colors.white
                ),
                decoration: InputDecoration(
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(5.0),
                  ),
                  filled: true,
                  fillColor: Colors.white10,

                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.7),
                  ),

                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(25.7),
                  ),
                  hintText: 'E-mail',
                ),
              ),

              SizedBox(height: 10,),
              TextFormField(
                obscureText: true,
                controller: _senhaController,
                style: TextStyle(
                  color: Colors.white
                ),
                decoration: InputDecoration(
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(5.0),
                  ),
                  filled: true,
                  fillColor: Colors.white10,

                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.7),
                  ),

                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(25.7),
                  ),
                  hintText: 'Senha',
                ),
              ),

              SizedBox(height: 10,),
              FlatButton(
                padding: EdgeInsets.all(10),
                child: Text('Avançar', 
                          style:TextStyle(fontSize: 20,
                        ),
                      ),
                color: Colors.red,
                onPressed: (){
                  if(_emailController.text != '' &&  _senhaController.text != ''){
                    auth.createAccount(_emailController.text, _senhaController.text).then((FirebaseUser user) {
                        Navigator.pushReplacementNamed(context, '/home');
                    }).catchError((e) => {
                        popup.open(context,e.code, e.message)
                    });
                  }else 
                    popup.open(context, 'Gostaria de enviar um presente a você! ','Por favor preencha os campos!');
                },
              ),
            ]
          ),
        ),
      );
  }
}
import 'package:flutter/material.dart';

class FinishedPage extends StatelessWidget{

  final int pts;

  const FinishedPage({ Key key, this.pts }) : super(key : key);

  @override
  Widget build(BuildContext context){
      return Scaffold(
          appBar: AppBar(
             title: Column(
                children: <Widget>[
                   Image.asset('assets/imgs/logo_appbar.png',height: 50,)
                ],
             ),
          ),

          body: Container(
             alignment: Alignment.center,
             color: Colors.black,
             padding: EdgeInsets.all(50),
             child: Column(
                children: <Widget>[
                    Text('Parabéns!!!',
                        style: TextStyle(
                           color: Colors.white,
                           fontWeight: FontWeight.bold,
                           fontSize: 40
                        )
                    ),

                    SizedBox(height: 40,),

                    Text('Você concluiu o quiz com sucesso.',
                        style: TextStyle(
                           color: Colors.white,
                           fontWeight: FontWeight.bold,
                           fontSize: 25
                        )
                    ),

                    SizedBox(height: 100,),

                    Text('Sua pontuação foi de $pts pts',
                        style: TextStyle(
                           color: Colors.white,
                           fontWeight: FontWeight.bold,
                           fontSize: 20
                        )
                    ),

                    SizedBox(height: 50,),

                    FlatButton(
                      child:Text('Continuar'),
                      color: Colors.red,
                      onPressed: (){
                         Navigator.pushReplacementNamed(context, '/home');
                      },
                    )

                ],
             ),
          ),
      );
  }
}
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:game/nav/finished.dart';
import 'package:game/nav/tente_novamente.dart';
import 'package:game/services/db/models.dart';


class QuizResistenciaPage extends StatelessWidget {

  final Game pergunta;
  final int pts;
  final int qtdPlayers;

  const QuizResistenciaPage({Key key, this.pergunta,this.pts, this.qtdPlayers = 0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Image.asset('assets/imgs/logo_appbar.png', height: 40,),
        ),

        bottomNavigationBar: this.qtdPlayers == 0 ? null : Container(
          decoration: new BoxDecoration (
              color: Colors.red,

          ),
          child: ListTile(
            contentPadding: EdgeInsets.only(top: 1, left: 5),
            leading: Icon(FontAwesomeIcons.gamepad),
            title: Text("Competindo com outros ${qtdPlayers - 1} jogadores"),
          ),
        ),

        body: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              
                Container(
                  padding: EdgeInsets.all(10),
                  alignment: Alignment.centerLeft,
                  color: Colors.redAccent,
                  height: MediaQuery.of(context).size.height / 11,
                  width: MediaQuery.of(context).size.width,
                  child:Row(
                    mainAxisAlignment:  MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Esportes',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold
                        ),
                      ),

                      Text('Pts: $pts',
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ],
                  ), 
                ),

                Container(
                  padding: EdgeInsets.all(30),
                  color: Colors.white54,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('Em que ano a Seleção Brasileira conquistou sua primeira medalha olímpica de futebol?', 
                        style: TextStyle( 
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold
                        ) 
                      )
                    ],
                  ),
                ),

                SizedBox(height: 20,),

                ButtonOpcoes(text: '2016',value:'2016',correctValue: '2016',),
                ButtonOpcoes(text: '2019',value:'2019',correctValue: '2016',),
                ButtonOpcoes(text: '2020',value:'2020',correctValue: '2016',),
                ButtonOpcoes(text: '2021',value:'2021',correctValue: '2016',),

            ],
          ),
        ),
    );
  }

}

class ButtonOpcoes extends StatelessWidget {
    const ButtonOpcoes({Key key, this.text, this.value,this.correctValue}) : super(key: key);    
    final String text;
    final String value;
    final String correctValue;

    @override
    Widget build(BuildContext context){
      return FlatButton(
        child: Text(text),
        color: Colors.white38,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(25.0),
        ),
        onPressed: () {
            if(value == correctValue){
                  Navigator.push(context, MaterialPageRoute(
                      builder: (BuildContext context) => FinishedPage(
                        pts: 5,
                      )
                    ),
                  );
            }else{
              Navigator.push(context, MaterialPageRoute(
                  builder: (BuildContext context) => TryAgainPage()
              ));
            }
        },
      );
    }
}
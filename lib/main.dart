import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:game/nav/nav.dart';
import 'package:game/services/authentication.dart';
import 'package:provider/provider.dart';

import 'nav/ranking.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<FirebaseUser>.value(value: AuthService().user)
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: true,
        navigatorObservers: [
          FirebaseAnalyticsObserver(analytics: FirebaseAnalytics()),
        ],
        routes: {
          '/': (context) => LoginPage(),
          '/home': (context) => HomePage(),
          '/profile': (context) => ProfilePage(),
          '/ranking': (context) => RankingPage(),
          '/cadastro': (context) => CadastroPage(),
          '/cadastro_email': (context) => CadastroEmailPage(),
          '/cadastro_quiz': (context) => CadastroQuizPage(),
          '/cadastro_quiz_two': (context) => CadastroQuizTwoPage(),
          '/cadastro_quiz_three': (context) => CadastroQuizThreePage(),
          '/quiz': (context) => QuizPage(),
          '/quiz_resistencia': (context) => QuizResistenciaPage(),
        },
        theme: ThemeData.dark(),
      ),
    );
  }
}
